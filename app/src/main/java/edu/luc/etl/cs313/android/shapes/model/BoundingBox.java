package edu.luc.etl.cs313.android.shapes.model;

import java.util.Iterator;
//import java.util.*;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		return f.getShape().accept(this);
		//return new Location(0, 0, f.getShape());
		//return null;
	}

	@Override
	public Location onGroup(final Group g) {
		int minX = 0;
		int minY = 0;
		int maxX = 0;
		int maxY = 0;

		for (Shape shape: g.getShapes()){
			Location location = shape.accept(this);
			Location shapeLocation = location.getShape().accept(this);
			Rectangle rectangle = (Rectangle)shapeLocation.getShape();

			if( minX == 0 || (location.getX() - (shapeLocation.getX() + rectangle.getWidth()/2)) <minX ){
				minX = (location.getX() - (shapeLocation.getX() + rectangle.getWidth()/2));
			}
			if( minY == 0 || (location.getY() - (shapeLocation.getY() + rectangle.getHeight()/2)) <minY ){
				minY = (location.getY() - (shapeLocation.getY() + rectangle.getHeight()/2));
			}
			if( maxX < (location.getX() + (shapeLocation.getX() + rectangle.getWidth())) ){
				maxX = (location.getX() + (shapeLocation.getX() + rectangle.getWidth()));
			}
			if( maxY < (location.getY() + (shapeLocation.getY() + rectangle.getHeight()))  ){
				maxY = (location.getY() + (shapeLocation.getY() + rectangle.getHeight()));
			}
		}
		return new Location(minX, minY, new Rectangle(maxX-minX, maxY-minY));
	}


	@Override
	public Location onLocation(final Location l) {
		//return l;
		return new Location(l.getX(), l.getY(), l.getShape().accept(this).getShape());
		//return null;
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		final int width = r.getWidth();
		final int height = r.getHeight();
		return new Location(0, 0, new Rectangle(width, height));
	}

	@Override
	public Location onStroke(final Stroke c) {
		return c.getShape().accept(this);
		//return new Location (0, 0, c.getShape());
		//return null;
	}

	@Override
	public Location onOutline(final Outline o) {
		return o.getShape().accept(this);
		//return new Location(0, 0, o.getShape());
		//return null;
	}

	@Override
	public Location onPolygon(final Polygon s) {
		Point[] points = (Point[]) s.getPoints().toArray();
		int i = 0;
		int[] xValues = new int[points.length], yValues = new int[points.length];
		for(Point point : points){
			xValues[i] = point.getX();
			yValues[i] = point.getY();
			i++;
		}

		int xMin = xValues[0], xMax = xValues[0];
		int yMin = yValues[0], yMax = yValues[0];
		for(int k = 1; k <i; k++){
			if(xValues[k]<xMin){
				xMin= xValues[k];
			}
			if(xValues[k]>xMax){
				xMax = xValues[k];
			}
			if(yValues[k] > yMax){
				yMax = yValues[k];
			}
		}
		//int x = xMin;
		//int y = yMin;
		return new Location(xMin, yMin, new Rectangle(xMax-xMin , yMax-yMin));
		//return null;
	}
}
